import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

class Login extends React.Component {
  constructor(props){
    super(props);
    this.state = {
    username:'',
    password:''
    }
   }
   onUsernameChange = (event) => {
     this.setState({username: event.target.value});
   }

   onPasswordChange = (event) => {
    this.setState({password: event.target.value});
  }

  submit = () => {
    let err = 'niste uneli'
    if (this.state.username.length>0 && this.state.password.length >0) {
      alert('cestitam ulogovan si')
    } else {
    if (this.state.username === '') {
      err += ' password'
    }
    if ((this.state.password === '')) {
      err += ' username'
    }
    alert(err);
  }
  }
   render() {
     return (
       <div>
         <MuiThemeProvider>
           <div>
             <TextField className="text-field" hintText="enter username" 
             floatingLabelText="username"
             onChange={this.onUsernameChange}/>
             <TextField className="text-field" hintText="enter Password" 
             floatingLabelText="Password"
             onChange={this.onPasswordChange}/>
           </div>

           <div>
             <RaisedButton label="login" primary={true} onClick={this.submit} />
           </div>
         </MuiThemeProvider>
       </div>
     )
   }
}

export default Login;